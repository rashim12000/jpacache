package com.f1soft;

import com.f1soft.dao.UserDAO;
import com.f1soft.entities.User;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * REST Web Service
 *
 * @author rashim.dhaubanjar
 */
@Path("generic")
@RequestScoped
public class GenericResource {

    @Inject
    private UserDAO userDAO;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of com.f1soft.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/addusers")
    @Produces(MediaType.APPLICATION_XML)
    public String insertUser() {
        User user1 = new User("asdf", "KTM");
        User user2 = new User("qwer", "BKT");
        User user3 = new User("sdfg", "PTN");
        System.out.println("userdao:::::::" + userDAO);
        userDAO.insertUser(user1);
        userDAO.insertUser(user2);
        userDAO.insertUser(user3);
        return "success";
    }

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUser() {
        List<User> user = new ArrayList<>();
        System.out.println("fetching users from 2 ");
//        int size = CacheManager.ALL_CACHE_MANAGERS.get(0)
//                .getCache("User").getSize();
//        System.out.println("cahce size  all users :::::::::2222222222222222" + size);
//
//        Cache cache = CacheManager.getInstance().getCache("User");

//////////        List<Object> object = cache.getKeys();
//        for (Object obj : cache.getKeys()) {
//            Element element = cache.get(obj);
//            if (element != null) {
//                System.out.println("user key::::::::::" + element.getObjectKey());
//                System.out.println("user object::::::::::" + element.getObjectValue());
//            }
//        }
//        if (!user.isEmpty()) {
//            return user;
//        }
        user = userDAO.getAllUser();
//        for (User u : user) {
//            User userId = new User();
//            userId.setId(u.getId());
//            cache.put(new Element(userId, u));
//        }
        return user;
    }

    @GET
    @Path("/userId/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public User getUserById(@PathParam("id") long id) {
        int size = CacheManager.ALL_CACHE_MANAGERS.get(0)
                .getCache("User").getSize();
        System.out.println("cahce size by id  :::::::::" + size);
        User userdata = new User();
        userdata.setId(id);
//        userdata.setUserName("qwer");
//        userdata.setUserAddress("KTM");
        Cache cache = CacheManager.getInstance().getCache("User");
//        System.out.println("cache:::::::::" + cache);
//        Cache cache = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache("com.f1soft.entities.User");
//        Cache cache = Cac
//        System.out.println("stat::::::" + cache.getStatistics());
//        List key_list = cache.getKeys();

//        List<Object> uu = cache.getKeys();
//        for (Object obj : uu) {
//            System.out.println("user :::::::::" + userdata);
//            System.out.println("object :::::::" + obj);
        Element element = cache.get(userdata);
        if (element != null) {
//            System.out.println("key from username :::::::::" + cache.get(userdata));
            System.out.println("key from username :::::::::" + element.getObjectValue());
//            System.out.println("key from object :::::::::" + cache.get(obj));
//            if (obj != null) {
//                System.out.println("id::::::::::::::::true");
//                System.out.println("Element key value :::::::::" + obj.toString());
////                System.out.println("element object value:::::::::" + element.getObjectValue().toString());

            return (User) element.getObjectValue();
//  }          
        }
//        }

//        System.out.println("cache:::::::" + cache);
        System.out.println("hit in databse");

        User user = userDAO.findById(id);
        cache.put(new Element(userdata, user));
        return user;
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }

    @POST
    @Path("/update/{id}")
    public boolean updateUser(@PathParam("id") long id, User user) {

        return userDAO.updateUser(id, user);
    }

}
