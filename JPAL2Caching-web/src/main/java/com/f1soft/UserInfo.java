package com.f1soft;

import com.f1soft.dao.UserDAO;
import com.f1soft.dao.impl.UserDAOImpl;
import com.f1soft.entities.User;

/**
 *
 * @author rashim.dhaubanjar
 */
public class UserInfo {

//    @Inject
//    private UserDAO userDAO;

    public void dataAccess() {
        UserDAO userDAO = new UserDAOImpl();
        User user = new User();
        User user1 = new User("Shyam", "KTM");
        User user2 = new User("Ram", "BKT");
        User user3 = new User("Sita", "PTN");
        System.out.println("userdao:::::::"+userDAO);
        userDAO.insertUser(user1);
        userDAO.insertUser(user2);
        userDAO.insertUser(user3);

//        user = userDAO.getAllUser();
//        System.out.println("users = " + user.toString());
    }

    public static void main(String[] args) {
        UserInfo info = new UserInfo();
        info.dataAccess();
    }
}
