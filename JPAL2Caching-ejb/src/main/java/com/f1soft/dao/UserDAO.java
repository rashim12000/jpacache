package com.f1soft.dao;

import com.f1soft.entities.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rashim.dhaubanjar
 */
@Local
public interface UserDAO {

    List<User> getAllUser();

    User findById(long id);
    
    void insertUser(User user);


    public boolean updateUser(long id, User user);

}
