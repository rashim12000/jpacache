package com.f1soft.dao.impl;

import com.f1soft.dao.UserDAO;
import com.f1soft.entities.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserDAOImpl implements UserDAO {

    @PersistenceContext(unitName = "CachePU")
    EntityManager em;

    @Override
    public List<User> getAllUser() {
        return em.createNamedQuery("User.findAll")
                .getResultList();
    }

    @Override
    public User findById(long id) {
        return (User) em.createNamedQuery("User.findById")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public void insertUser(User user) {
        System.out.println("em:::::" + em);
        em.persist(user);
    }

    @Override
    public boolean updateUser(long id, User data) {
        User user =  findById(id);
        user.setUserName(data.getUserName());
        user.setUserAddress(data.getUserAddress());
        em.merge(user);
        return true;
    }
}
