package com.f1soft.entities;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author rashim.dhaubanjar
 */
@Data
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "com.f1soft.entities.User")
@Entity
@Table(name = "USERINFO")
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "select u from User u ", hints = {
        @QueryHint(name = "org.hibernate.cacheable", value = "true")}),
    @NamedQuery(name = "User.findById", query = "select u from User u where u.id = :id")
})
@XmlRootElement(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = -7392726925440390214L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "USER_ADDRESS")
    private String userAddress;

    public User() {
    }

    public User(long id) {
        this.id = id;
    }

    public User(String userName, String userAddress) {
        this.userName = userName;
        this.userAddress = userAddress;
    }

//    @Override
//    public int hashCode() {
////        int hash = 7;
////        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
//        return (int) this.id;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final User other = (User) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        return true;
//    }
}
